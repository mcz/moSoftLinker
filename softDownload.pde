// software made by Mocatz under CC
// date 08/11/2014

import controlP5.*;

ControlP5 cp5;
int nbLinks = 10;
String[] webLink = new String[nbLinks];
String[] name = new String[nbLinks];
int posXBt, posYBt;
boolean readyToLink = false;

void setup()
{
  size (600, 400);
  background(0);
  cp5 = new ControlP5(this);
  posXBt = 20;
  posYBt = 50;
  for (int i = 0; i < nbLinks; i++)
  {
    name[i] = "web" + i;
    webLink[i] = "http://instant-ephemere.com/jcourat";
  }
  for (int i = 0; i < nbLinks; i++)
  {
    cp5.addButton(name[i])
      .setValue(0)
        .setPosition(posXBt, posYBt)
          .setSize(200, 19)
            ;
    posYBt += 25;
  }

  text("SoftDownload | software workflow webpage linker", 10, 20);
  readyToLink = true;
}



void draw()
{
}

public void controlEvent(ControlEvent theEvent) 
{
  //println(theEvent.getController().getName());
  String linkTo = theEvent.getController().getName();
  println(linkTo);
  if (readyToLink == true)
  {
    if ( linkTo == name[0])
    {
      println("---------------------------------------->");
      link ("http://instant-ephemere.com/jcourat");
    }
  }
}

