// software made by Mocatz under CC
// date 08/11/2014
JSONArray data;
int nbLinks;
String[] webLink;
String[] name;
int[] categorie;
CheckBox[] myCheckBox;
int posXBt, posYBt;
int posXBtValid, posYBtValid, longBtValid, largBtValid;
int posXBtSelectAll, posYBtSelectAll, longBtSelectAll, largBtSelectAll;
boolean readyToLink = false;
boolean selectAll = true;
color kuler;
int posColums;
void setup()
{
  size (800, 400);
  background(0);
  posXBt = 20;
  posYBt = 50;
  longBtValid = 50;
  largBtValid = 20;
  posXBtValid = width - longBtValid - 20 ;
  posYBtValid = 360;

  posXBtSelectAll = 20;
  posYBtSelectAll = 360;
  longBtSelectAll = 50;
  largBtSelectAll = 20;
  //-- Load Json data
  data = loadJSONArray("links.json");  
  println(data.size());
  nbLinks = data.size();
  webLink = new String[nbLinks];
  name = new String[nbLinks];
  categorie = new int[nbLinks];
  myCheckBox = new CheckBox[nbLinks];
  for (int i = 0; i < nbLinks; i++)
  {
    JSONObject link = data.getJSONObject(i); 

    categorie[i]= link.getInt("cat");
    name[i]     = link.getString("name");
    webLink[i]  = link.getString("webLink");
  }
  //linkList();
  //-- Create the Checkbox colums
  int ligne = 0;
  for (int i = 0; i < nbLinks; i++)
  {
    if (categorie[i] ==0) { 
      kuler = #346EE8;
    } else if (categorie[i] ==1) { 
      kuler = #E83437;
    } else if (categorie[i] ==2) { 
      kuler = #48E834;
    } else { 
      kuler = #E8BE34;
    }
    myCheckBox[i] = new CheckBox(posXBt + categorie[i] * 200, posYBt + (ligne*20), name[i], kuler);
    int previousCat = categorie[i];
    if (i < nbLinks-1)
    {
      if (previousCat == categorie[i+1])
      {
        ligne ++;
      } else
      {
        ligne = 0;
      }
    }
  }
  readyToLink = true;
  posColums =0;
}



void draw()
{
  background(0);
  fill(230);
  text("SoftDownload | software workflow webpage linker", 20, 20);
  pushMatrix();
  translate(posColums, 0);
  for (int i = 0; i < nbLinks; i++)
  { 
    myCheckBox[i].display();
  }
  popMatrix();
  //-- select all
  fill(150);
  rect(posXBtSelectAll, posYBtSelectAll, longBtSelectAll, largBtSelectAll);
  fill(0);
  if (selectAll)
  {
    text ("All", posXBtSelectAll + 5, posYBtSelectAll + 15);
  } else
  {
    text ("None", posXBtSelectAll + 5, posYBtSelectAll + 15);
  }
  //-- valid selection BT
  fill(0, 200, 0);
  rect(posXBtValid, posYBtValid, longBtValid, largBtValid);
  fill(0);
  text ("Load", posXBtValid + 5, posYBtValid + 15);
}
void keyPressed()
{
  if (key == CODED) 
  {
    if (keyCode == LEFT) 
    {
      posColums -= 200;
    } else if (keyCode == RIGHT) 
    {
      posColums += 200;
    }
  }
}
void mouseReleased()
{
  selector();
}

void selector()
{
  for (int i = 0; i < nbLinks; i++)
  {
    myCheckBox[i].mouseChecked(mouseX, mouseY);
  }
  //-- valid selection BT
  if (mouseX > posXBtSelectAll && mouseX < posXBtSelectAll + longBtSelectAll && mouseY > posYBtSelectAll && mouseY < posYBtSelectAll + largBtSelectAll)
  {
    //-- select or un select all items
    if (selectAll)
    {
      for (int i = 0; i < nbLinks; i++)
      {
        myCheckBox[i].setSelect();
      }
      selectAll = false;
    } else
    {
      for (int i = 0; i < nbLinks; i++)
      {
        myCheckBox[i].setUnselect();
      }
      selectAll = true;
    }
  }
  //-- valid selection BT
  if (mouseX > posXBtValid && mouseX < posXBtValid + longBtValid && mouseY > posYBtValid && mouseY < posYBtValid + largBtValid)
  {
    listLinker();
  }
}

void listLinker()
{
  for (int i = 0; i < nbLinks; i++)
  {
    boolean tState = myCheckBox[i].getState(); 
    if (tState)
    {
      link( webLink[i]);
    }
  }
}

