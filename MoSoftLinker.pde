// software made by Mocatz under CC
// date 08/11/2014
// mettre le % de soft selectionné
// mettre le % de soft selectionné par section

PFont font;
JSONArray data, nameOfTheCategorie;

int nbLinks; // nombre de lien au total
int nbCat;  // nombre de catégories (les catégories sont représenté par un entier  cat 0, cat1 etc
String[] webLink;
String[] webLinkDownLoad;
String[] name; 
int[] categorie;
CheckBox[] myCheckBox;
BoxItem[] myBoxItem;
BoxItem downloadMe = new BoxItem ( 620, 360, "getTheZip", #FF0000, false);
String[] nameCategorie;
int posXBt, posYBt;
Pourcentage percentTotalApp;
Pourcentage[] percent;
Pourcentage[] percent1;
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ 
//--button selectAll / NONE and LOAD
int posXBtValid, posYBtValid, longBtValid, largBtValid;
int posXBtSelectAll, posYBtSelectAll, longBtSelectAll, largBtSelectAll;
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
boolean readyToLink = false;
boolean selectAll = true;
boolean showInfo = false;
boolean webOrDownload = false;
color kuler;
int theColorIs;
int posColums;
int nbApp;
int posXCursor, posYCursor;
////////////////////////////////////////////////////////////////
void setup()
{
  size (800, 400);
  background(0);
  smooth();
  posXBt = 20;
  posYBt = 70;
  longBtValid = 50;
  largBtValid = 20;
  posXBtValid = width - longBtValid - 20 ;
  posYBtValid = 360;

  posXBtSelectAll = 20;
  posYBtSelectAll = 360;
  longBtSelectAll = 50;
  largBtSelectAll = 20;
  //-- Load Json data
  data = loadJSONArray("links.json");  
  nameOfTheCategorie = loadJSONArray("categorieName.json");
  println(data.size());
  nbLinks = data.size();
  webLink = new String[nbLinks];
  webLinkDownLoad = new String[nbLinks];
  name = new String[nbLinks];
  categorie = new int[nbLinks];
  myCheckBox = new CheckBox[nbLinks];

  theColorIs = 0;

  //allItem = new CheckBox (posXBt, posYBt - 25, "selectItem", #346EE8, false);
  for (int i = 0; i < nbLinks; i++)
  {
    JSONObject link = data.getJSONObject(i); 
    categorie[i]= link.getInt("cat");
    nbCat =  categorie[i] + 1;
    name[i]     = link.getString("name");
    webLink[i]  = link.getString("webLink");
    webLinkDownLoad[i] = link.getString("webDownload");
  }
  println("nbCat = " + nbCat);
  myBoxItem = new BoxItem[nbCat];
  nameCategorie = new String[nbCat];
  percent = new Pourcentage[nbCat];
  percent1 = new Pourcentage[nbCat];
  for (int i = 0; i < nbCat; i++)
  {
    JSONObject link = nameOfTheCategorie.getJSONObject(i); 
    nameCategorie[i] = link.getString("cat" + str(i));
  }
  chooseTheColor();
  posXCursor = posXBt + 70;
  posYCursor = 370;
  for (int i = 0; i < nbCat; i++)
  {
    chooseTheColor();
    println("the color is at start : " + theColorIs);
    myBoxItem[i] = new BoxItem(posXBt + (i * 200), posYBt - 25, nameCategorie[i], kuler, false);
    percent1[i] = new Pourcentage(posXBt + 60 + (i * 30), 360, 20, 20, kuler, "circle");
    percent[i] = new Pourcentage(posXBt + (i * 200), posYBt - 35, 150, 5, kuler, "rect");
    println("the color is : " + theColorIs);
    if (theColorIs > 2)
    {
      theColorIs = 0;
    } else
    {
      theColorIs++;
    }
    chooseTheColor();
  }
  //linkList();
  //-- Create the Checkbox colums
  theColorIs = 0;
  int ligne = 0;
  for (int i = 0; i < nbLinks; i++)
  {
    if (theColorIs ==0) { 
      kuler = #346EE8;
    } else if (theColorIs ==1) { 
      kuler = #E83437;
    } else if (theColorIs ==2) { 
      kuler = #48E834;
    } else { 
      kuler = #E8BE34;
    }
    myCheckBox[i] = new CheckBox(posXBt + categorie[i] * 200, posYBt + (ligne*22), name[i], kuler, true);
    int previousCat = categorie[i];
    if (i < nbLinks-1)
    {
      if (previousCat == categorie[i+1])
      {
        ligne ++;
      } else
      {
        ligne = 0;
        println("next colums");
        if (theColorIs > 2)
        {
          theColorIs = 0;
        } else
        {
          theColorIs++;
        }
      }
    }
  }
  readyToLink = true;
  posColums =0;
  // The font must be located in the sketch's 
  // "data" directory to load successfully
  font = loadFont("SquareFont-12.vlw");
  textFont(font, 12);
  println(categorie);
  
  percentTotalApp = new Pourcentage (posXBt, 350, width - 40, 5, #717171, "rect");
  //percent1 = new Pourcentage (posXBt + (i * 200), 300, 200, 5, kuler, "circle");
  //percent1 = new Pourcentage (posXBt, 300, 30, 30, #ffffff, "circle");
}



void draw()
{
  background(0);
  fill(#717171);
  text("MoSoftLinker | software workflow webpage linker", 20, 20);
  text(nbApp + " / " + nbLinks + " " + "selected", 20, 345);
  pushMatrix();
  translate(posColums, 0);
  for (int i = 0; i < nbCat; i++)
  { 
    int nbAppList = 0; 
    int selectedInList = 0;
    myBoxItem[i].display();
    for (int j = 0; j < nbLinks; j++)
    {
       if(categorie[j] == i)
      {
         nbAppList += 1;
         if (myCheckBox[j].getState())
         {
             selectedInList +=1;
         }
      } 
    }
    percent[i].display(selectedInList, nbAppList);
    //percent1[i].display(selectedInList, nbAppList);
  }
  for (int i = 0; i < nbLinks; i++)
  { 
    myCheckBox[i].display();
  }
  popMatrix();
  for (int i = 0; i < nbCat; i++)
  { 
    int nbAppList = 0; 
    int selectedInList = 0;
    for (int j = 0; j < nbLinks; j++)
    {
       if(categorie[j] == i)
      {
         nbAppList += 1;
         if (myCheckBox[j].getState())
         {
             selectedInList +=1;
         }
      } 
    }
    stroke(#717171,100);
    percent1[i].display(selectedInList, nbAppList);
  }
  //-- select all
  fill(#717171,100);
  //noFill();
  rect(posXBtSelectAll, posYBtSelectAll, longBtSelectAll, largBtSelectAll);
  fill(#717171,200);
  if (selectAll)
  {
    text ("All", posXBtSelectAll + 5, posYBtSelectAll + 15);
  } else
  {
    text ("None", posXBtSelectAll + 5, posYBtSelectAll + 15);
  }
  //-- valid selection BT
  fill(0, 200, 0,100);
  rect(posXBtValid, posYBtValid, longBtValid, largBtValid);
  fill(0);
  text ("Load", posXBtValid + 5, posYBtValid + 15);
  downloadMe.display();
  percentTotalApp.display(nbApp, nbLinks);
  fill(150);
  ellipse(posXCursor, posYCursor, 3, 3);
  //percent1.display(nbApp, nbLinks);
  if (showInfo)
  {
    info();
  }
}
////////////////////////////////////////////////////////////////
void keyPressed()
{
  if (key == CODED) 
  {
    if (keyCode == RIGHT) 
    {
      posColums -= 200;
      posXCursor += 30;
    } else if (keyCode == LEFT) 
    {
      posColums += 200;
      posXCursor -= 30;
    }
  }
  if (keyCode == TAB) 
  {
    posColums = 0;
    posXCursor = posXBt + 70;
  } else if (key == 'i')
  {
    showInfo =! showInfo;
  } else if (key == 'a')
  {
    for (int i = 0; i < nbLinks; i++)
    {
      if ( categorie[i] == 1)
      {
        myCheckBox[i].setSelect();
      }
    }
  }
}
////////////////////////////////////////////////////////////////
void mousePressed()
{
}
void mouseReleased()
{
  checkTheBox();
  selector(); 
  downloadMe.mouseChecked(mouseX, mouseY);
  for (int i = 0; i < nbLinks; i++)
  {
    if (myCheckBox[i].getState())
    {
      nbApp +=1;
    }
  }
}
////////////////////////////////////////////////////////////////
void checkTheBox()
{
  if (mouseY < posYBt)
  {
    for (int i = 0; i < nbCat; i++)
    {
      myBoxItem[i].mouseChecked(mouseX, mouseY);
      selectOneCategorie();
    }
  }
  nbApp = 0;
  for (int i = 0; i < nbLinks; i++)
  {
    myCheckBox[i].mouseChecked(mouseX, mouseY);
  }
}
////////////////////////////////////////////////////////////////
void selector()
{   
  //-- valid selection BT
  if (mouseX > posXBtSelectAll && mouseX < posXBtSelectAll + longBtSelectAll && mouseY > posYBtSelectAll && mouseY < posYBtSelectAll + largBtSelectAll)
  {
    //-- select or un select all items
    if (selectAll)
    {
      for (int i = 0; i < nbLinks; i++)
      {
        myCheckBox[i].setSelect();
      }
      for (int i = 0; i < nbCat; i++)
      {
        myBoxItem[i].setSelect();
      }
      selectAll = false;
    } else
    {
      for (int i = 0; i < nbLinks; i++)
      {
        myCheckBox[i].setUnselect();
      }
      for (int i = 0; i < nbCat; i++)
      {
        myBoxItem[i].setUnselect();
      }
      selectAll = true;
    }
  }
  //-- valid selection BT
  if (mouseX > posXBtValid && mouseX < posXBtValid + longBtValid && mouseY > posYBtValid && mouseY < posYBtValid + largBtValid)
  {
    listLinker();
  }
}
////////////////////////////////////////////////////////////////
void listLinker()
{

  if (downloadMe.getState())
  {
    webOrDownload = true;
  } else
  {
    webOrDownload = false;
  }
  for (int i = 0; i < nbLinks; i++)
  {
    boolean tState = myCheckBox[i].getState(); 
    if (tState)
    {
      if (webOrDownload)
      {
        link(webLinkDownLoad[i]);
      } else
      {
        link( webLink[i]);
      }
    }
  }
}
////////////////////////////////////////////////////////////////
void selectOneCategorie()
{

  for (int i = 0; i < nbCat; i++)
  {
    
    if (myBoxItem[i].getTheCat(mouseX, mouseY))
    {
      myBoxItem[i].mouseChecked(mouseX, mouseY);
      if (myBoxItem[i].getState())
      {
        for (int j = 0; j < nbLinks; j++)
        {
          if (categorie[j] == i)
          {
            myCheckBox[j].setSelect();
          }
        }
      } else
      {
        for (int j = 0; j < nbLinks; j++)
        {
          if (categorie[j] == i)
          {
            myCheckBox[j].setUnselect();
          }
        }
      }
    }
  }
}
////////////////////////////////////////////////////////////////
void chooseTheColor()
{
  if (theColorIs ==0) { 
    kuler = #346EE8;
  } else if (theColorIs ==1) { 
    kuler = #E83437;
  } else if (theColorIs ==2) { 
    kuler = #48E834;
  } else if (theColorIs ==3) { 
    kuler = #E8BE34;
  } else { 
    kuler = #ffffff;
  }
}

