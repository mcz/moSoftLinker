class BoxItem
{
  float posX;
  float posY;
  boolean state;
  String boxName;
  color  boxColor;
  boolean style;

  BoxItem(float mposX, float mposY, String mBoxName, color mBoxColor, boolean mStyle)
  {
    posX = mposX;
    posY = mposY;
    state = false;
    boxName = mBoxName;
    boxColor = mBoxColor;
    style = mStyle;
  }

  void display()
  {
    if (style)
    {
      fill(boxColor);
      noStroke();
      text(boxName, posX + 20 + 10, posY + 20 - (20/4)); 
      rect (posX, posY, 20, 20);
      stroke(0);
      if (state)
      {
        line (posX, posY, posX + 20, posY + 20);
        line (posX, posY + 20, posX + 20, posY);
      }
    } else
    {
      stroke (boxColor);
      fill(boxColor);
      text(boxName, posX + 20 + 10, posY + 20 - (20/4)); 
      noFill();
      rect (posX, posY, 20, 20);
      if (state)
      {
        fill(boxColor);
        ellipse(posX + 20 /2, posY + 20 /2, 10, 10);
      }
    }
  }

  void mouseChecked(int x, int y)
  {
    if (x > posX  + posColums && x < posX + 20 + posColums && y > posY && y < posY + 20)
    {
      state =! state;
    }
  }
  boolean getTheCat(int x, int y)
  {
    if (x > posX  + posColums && x < posX + 20 + posColums && y > posY && y < posY + 20)
    {
      return true;
    }else
    {
       return false; 
    }
  }

  boolean getState()
  {
    return state;
  }

  void setSelect()
  {
    state = true;
  }
  void setUnselect()
  {
    state = false;
  }
}

