class Pourcentage
{
  float posX, posY;
  int longR, largR;
  color boxColor;
  String style;

  Pourcentage(float mposX, float mposY, int mLongR, int mLargR, color mBoxColor, String mStyle)
  {
    posX = mposX;
    posY = mposY;
    longR = mLongR;
    largR = mLargR;
    boxColor = mBoxColor;
    style = mStyle;
  }

  void display(int nbApp, int totalApp)
  {
    if (style == "rect")
    {
      float longRForApp;
      if (nbApp == totalApp)
      {
        longRForApp = longR;
      } else
      {
        longRForApp = (longR / totalApp) * nbApp;
      }
      stroke(boxColor);
      noFill();
      rect(posX, posY, longR, largR);
      fill(boxColor, 200);
      rect(posX, posY, longRForApp, largR);
    } else if (style == "circle")
    {
      float degreePercent;
      if (nbApp == totalApp)
      {
        degreePercent = 360;
      } else
      {
        degreePercent = (360 / totalApp) * nbApp;
      }
      noFill();
      ellipse(posX + (longR/2), posY + (largR/2),longR +1, largR +1);
      float deg = degreePercent;
      float rad = radians(deg);
      fill(boxColor);
      arc(posX + (longR/2), posY + (largR/2), longR, largR, 0, rad);
      fill(0);
      noStroke();
      ellipse(posX + (longR/2), posY + (largR/2),longR/2, largR/2);
    }
  }
}

